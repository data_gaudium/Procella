About
=====

Package for weather and urban energy use.

Quickstart
----------

Run the script:

    cd data && ./prepare_data.sh

This may take some time.

The building data interface and example useage is given in:

    ./data/building_data_interface.py

And the netCDF weather data interface and example useage is given in:

    ./data/weather_data_interface.py

Depends
-------

- numpy
- scipy
- pandas
- netCDF4
- geopy
