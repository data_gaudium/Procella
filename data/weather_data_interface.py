#!/usr/bin/env python
#-*- coding: utf-8 -*-
#==============================================================================
# Copyright (c) 2018, The Members of Data Gaudium
# All rights reserved.
#
# DESCRIPTION:
# Interface for accessing weather data in the netCDF4 format
#
# OPTIONS: --
# AUTHOR: Data Gaudium
# CONTACT:
#==============================================================================
from __future__ import print_function, division, absolute_import
from six import iteritems
import os
from functools import wraps
from geopy.distance import geodesic
import pandas as pd
from netCDF4 import Dataset
import numpy as np
from scipy.interpolate import interp2d, LinearNDInterpolator
import parse_csv_data as cvs_parsers
from building_data_interface import BuildingGenerator

# default global file name locations
dir_path = os.path.dirname(os.path.relpath(__file__))
if dir_path:
    dir_path += "/"
weather_nc_file_dflt = dir_path + "./SMC data challenge - Jibo/WRF-weather-output-one month/WRF-Output/wrfchibldg90m04-2015.nc"


def ncdump(nc_fid, verb=True):
    """
    @brief Outputs dimensions, variables and their attribute information.
    The information is similar to that of NCAR's ncdump utility.
    ncdump requires a valid instance of Dataset.
    Original Author: Chris Slocum, 3/20/2014
    @param nc_fid : netCDF4.Dataset
        A netCDF4 dateset object
    @param verb : Boolean
        whether or not nc_attrs, nc_dims, and nc_vars are printed
    @return
        nc_attrs : list
            A Python list of the NetCDF file global attributes
        nc_dims : list
            A Python list of the NetCDF file dimensions
        nc_vars : list
            A Python list of the NetCDF file variables
    """
    def print_ncattr(key):
        """
        Prints the NetCDF file attributes for a given key
        """
        try:
            print("\t\ttype:", repr(nc_fid.variables[key].dtype))
            for ncattr in nc_fid.variables[key].ncattrs():
                print('\t\t%s:' % ncattr,\
                      repr(nc_fid.variables[key].getncattr(ncattr)))
        except KeyError:
            print("\t\tWARNING: %s does not contain variable attributes" % key)

    # NetCDF global attributes
    nc_attrs = nc_fid.ncattrs()
    if verb:
        print("NetCDF Global Attributes:")
        for nc_attr in nc_attrs:
            print('\t%s:' % nc_attr, repr(nc_fid.getncattr(nc_attr)))
    nc_dims = [dim for dim in nc_fid.dimensions]  # list of nc dimensions
    # Dimension shape information.
    if verb:
        print("NetCDF dimension information:")
        for dim in nc_dims:
            print("\tName:", dim)
            print("\t\tsize:", len(nc_fid.dimensions[dim]))
            print_ncattr(dim)
    # Variable information.
    nc_vars = [var for var in nc_fid.variables]  # list of nc variables
    if verb:
        print("NetCDF variable information:")
        for var in nc_vars:
            if var not in nc_dims:
                print('\tName:', var)
                print("\t\tdimensions:", nc_fid.variables[var].dimensions)
                print("\t\tsize:", nc_fid.variables[var].size)
                print_ncattr(var)
    return nc_attrs, nc_dims, nc_vars


def field_augment(fn):
    """!
    @brief Automatically adds baseline values to certain
    fields.
    """
    BASELINE_T = 290.  # K
    BASELINE_P = 1.e5  # Pa
    @wraps(fn)
    def wrapper(self, *args, **kwargs):
        if args[0] == "T":
            return fn(self, *args, **kwargs) + BASELINE_T
        elif args[0] == "P":
            return fn(self, *args, **kwargs) + BASELINE_P
        else:
            # pass through
            return fn(self, *args, **kwargs)
    return wrapper


class WeatherData(object):
    def __init__(self, nc_file=weather_nc_file_dflt, year_offset=-2):
        self.nc_file = nc_file
        self.all_int_times = self.get_times(t0='1/1/1990', tf='1/1/2020', \
                out_fmt="int", year_offset=year_offset)
        self.lat_coords = self.get_field_at_time_idx("XLAT", 0)
        self.long_coords = self.get_field_at_time_idx("XLONG", 0)

    def get_times(self, t0='4/1/2017 00:00:00', tf='4/2/2017 03:45:00', out_fmt="int", year_offset=-2):
        """!
        @brief Get available times in the weather nc datafile
        """
        t0 = cvs_parsers.date_to_int(t0)
        tf = cvs_parsers.date_to_int(tf)
        # convert strings to ints or datetime
        np_time_data = self._get_field_all("Times")
        np_time_data = [self._date_cleaner(char_array) for char_array in np_time_data]
        np_time_data = [pd.to_datetime(date_str) - pd.DateOffset(years=year_offset) if date_str else None \
                        for date_str in np_time_data]
        # optionally convert to ints
        if out_fmt == "int":
            np_time_data = [cvs_parsers.datetime_to_int(date) if date else np.nan \
                            for date in np_time_data]
        return np.array(np_time_data)

    def _date_cleaner(self, char_array_date):
        date_str = ''.join([char for char in char_array_date])
        return date_str.replace('_', ' ')

    def get_neighbor_time_idxs(self, t):
        """!
        @brief Helper function for linear interpolation of field data in time.
        @return (bounding time indices), (weights)
        """
        t = cvs_parsers.date_to_int(t)
        idx_1 = np.searchsorted(self.all_int_times, t)  # upper index
        idx_0 = idx_1 - 1  # lower bound index
        # compute weights for linear time interpolation
        t0, tf = self.all_int_times[idx_0], self.all_int_times[idx_1]
        w_f = float((t - t0) / (tf - t0))
        w_0 = 1. - w_f
        try:
            assert 0 <= w_f <= 1
            assert 0 <= w_0 <= 1
        except:
            if idx_0 < 0:
                idx_0, idx_1 = 0, 0
                w_f, w_0 = 1., 0.
        return (idx_0, idx_1), (w_0, w_f)

    @field_augment
    def _get_field_all(self, field_name):
        """!
        @brief Get all field data with name field_name.
            WARNING: this might require a lot of RAM. Consider using
            get_field_at_time_idx to slice out a small portion of the data
        @param field_name str. name of field to extract from nc file
        """
        self._verify_field_name(field_name)
        with Dataset(self.nc_file, 'r') as nc_dataset:
            np_field_data = nc_dataset[field_name][:]
        return np_field_data

    @field_augment
    def get_field_at_time_idx(self, field_name, time_idx):
        """!
        @brief Slice field data out of nc file at specified times
        """
        self._verify_field_name(field_name)
        with Dataset(self.nc_file, 'r') as nc_dataset:
            np_field_data = nc_dataset[field_name][time_idx, :]
        return np_field_data

    def _get_valid_names(self):
        nc_vars = None
        with Dataset(self.nc_file, 'r') as nc_dataset:
            nc_attrs, nc_dims, nc_vars = ncdump(nc_dataset)
        return nc_vars

    def _verify_field_name(self, field_name):
        if field_name not in self._get_valid_names():
            raise KeyError

    def _gen_field_interp_2d(self, field_z):
        points = np.array([self.lat_coords.flatten(), self.long_coords.flatten()]).T
        return LinearNDInterpolator(points, field_z.flatten(), rescale=True)

    def get_spatial_field(self, field_name, time_list=['4/1/2017 04:00:00', '4/1/2017 04:20:00'], z_lvl=0):
        """!
        @breif Retrive 2d fields at given times.
        @param time_list list of times in int or datetime format
        @return list of 2d fields (np 2d arrays) ordered by (LATITUDE, LONGITUDE)
        """
        out_fields = []
        neighbor_idx_list, neighbor_wgt_list = [], []
        for time in time_list:
            time = cvs_parsers.date_to_int(time)
            neighbor_t_idx, neighbor_wgt = self.get_neighbor_time_idxs(time)
            neighbor_idx_list.append(neighbor_t_idx)
            neighbor_wgt_list.append(neighbor_wgt)
        neighbor_idx_list = np.array(neighbor_idx_list)
        neighbor_wgt_list = np.array(neighbor_wgt_list)
        # larger reads than previous impl
        field_t0_list = self.get_field_at_time_idx(field_name, neighbor_idx_list[:, 0])
        field_tf_list = self.get_field_at_time_idx(field_name, neighbor_idx_list[:, 1])
        for i, wgt in enumerate(neighbor_wgt_list):
            out_fields.append(field_t0_list[i] * wgt[0] + field_tf_list[i] * wgt[1])
        out_fields = np.array(out_fields)
        if len(out_fields.shape) == 4:
            return out_fields[:, z_lvl, :, :]
        return out_fields

    def get_field_at_loc(self, field_name, lat_long, time_list=['4/1/2017 04:00:00', '4/1/2017 04:20:00'], z_lvl=0):
        """!
        @brief Obtain the field as a function of time at the
        supplied lat_long coordinate pair.
        @param lat_long tuple of floats. (latitude, longitude)[deg] pair
        @param time_list list of times in int or datetime format
        """
        out_fields = []
        field_slices = self.get_spatial_field(field_name, time_list, z_lvl)
        for field_slice in field_slices:
            try:
                n_nodes = self._nearest_lat_long_nodes(lat_long)
                field_at_neighbor_nodes = np.array([field_slice[node] for node in n_nodes])
                dist_to_neighbor_nodes = \
                    np.array([geodesic((self.xlat[node], self.xlong[node]), lat_long).meters for node in n_nodes])
                field_value = np.average(field_at_neighbor_nodes, weights=1./dist_to_neighbor_nodes)
            except:
                field_interp = self._gen_field_interp_2d(field_slice)
                field_value = field_interp(lat_long[0], lat_long[1])
            out_fields.append(field_value)
        return np.array(out_fields)

    def _nearest_lat_long_nodes(self, lat_long):
        lat_coord = lat_long[0]
        long_coord = lat_long[1]
        idx_lat_up = np.searchsorted(self.xlat[:, 0], lat_coord)  # upper latitude index
        idx_long_up = np.searchsorted(self.xlong[0, :], long_coord)  # upper latitude index
        assert not np.isnan(idx_lat_up)
        assert not np.isnan(idx_long_up)
        idx_lat_down = idx_lat_up - 1
        idx_long_down = idx_long_up - 1
        return ((idx_lat_down, idx_long_down), (idx_lat_down, idx_long_up), \
                (idx_lat_up, idx_long_down), (idx_lat_up, idx_long_up))

    @property
    def xlat(self):
        return self.lat_coords

    @property
    def xlong(self):
        return self.long_coords


if __name__ == "__main__":
    # open weather data in read only mode
    weather_data_nc = Dataset(weather_nc_file_dflt, 'r')
    nc_attrs, nc_dims, nc_vars = ncdump(weather_data_nc)
    weather_data_nc.close()

    # load a building
    bg = BuildingGenerator()
    all_avail_ids = bg.building_ids
    my_build_id = all_avail_ids[213]
    # make a building instance
    my_building = bg.gen_building(my_build_id)

    # get field T (temperature in K) at 2m above the ground
    t_list = ['4/1/2015 04:20:00']
    wd = WeatherData()
    temp = wd.get_spatial_field("T2", t_list)[0]
    xlong, xlat = wd.xlong, wd.xlat

    # plot 2d temp field
    import matplotlib.pyplot as plt
    plt.figure()
    plt.pcolor(xlong, xlat, temp)
    plt.colorbar(label='Temperature @2m [K]')
    plt.scatter(my_building.centroid[1], my_building.centroid[0], c='k')
    plt.annotate('Bld ID: ' + str(my_building.id), \
                 xy=(my_building.centroid[1], my_building.centroid[0]))
    plt.xlabel("Longitude W [deg]")
    plt.ylabel("Latitude N [deg]")
    plt.title("Time : %s" % t_list[0])
    plt.savefig('temp_2d.png')
    plt.close()

    # get temperature at longitude coords of building
    t0, tf = '4/1/2017', '4/2/2017 03:00:00'
    e_times = my_building.energy_interface.get_times(t0=t0, tf=tf)
    temp_at_building = wd.get_field_at_loc("T2", my_building.centroid, e_times)
    print("Temperature at building at %s = %s" % (t_list[0], str(temp_at_building)))
    plt.figure()
    plt.scatter((e_times - np.min(e_times)) / 60. / 60. / 24., temp_at_building, label="Temperature [K]", s=3)
    plt.xlabel("Time [days]")
    plt.ylabel("Outside Temperature [K]")
    plt.title(r"Bld ID=%s,  $t_0$= %s,  $t_f$= %s" % (str(my_building.id), str(t0), str(tf)))
    plt.legend()
    plt.savefig("ex_temp_t.png")
    plt.close()

    # ex gas and elec use vs time for one month
    times = my_building.energy_interface.get_times(t0, tf)
    time_deltas = my_building.energy_interface.get_time_deltas(t0, tf)
    gas = my_building.energy_interface.get_field("gas_timestep", t0, tf)
    elec = my_building.energy_interface.get_field("elec_timestep", t0, tf)
    # data was recorded in 10 minute intervals
    delta_t_s = time_deltas

    plt.figure()
    plt.scatter((times - np.min(times)) / 60. / 60. / 24., gas / delta_t_s / 1000., label="Gas use [kW]", s=3)
    plt.legend()
    plt.title(r"Bld ID=%s,  $t_0$= %s,  $t_f$= %s" % (str(my_building.id), str(t0), str(tf)))
    plt.xlabel("time [days]")
    plt.ylabel("Energy Use [kW]")
    plt.savefig("ex_gas_t.png")
    plt.close()
    plt.figure()
    plt.scatter((times - np.min(times)) / 60. / 60. / 24., elec / delta_t_s / 1000., label="Elec use [kW]", s=3)
    plt.legend()
    plt.title(r"Bld ID=%s,  $t_0$= %s,  $t_f$= %s" % (str(my_building.id), str(t0), str(tf)))
    plt.ylabel("Energy Use [kW]")
    plt.xlabel("time [days]")
    plt.savefig("ex_elec_t.png")
    plt.close()
