#!/usr/bin/env python
#-*- coding: utf-8 -*-
#==============================================================================
# Copyright (c) 2018, The Members of Data Gaudium
# All rights reserved.
#
# DESCRIPTION:
# Interface for accessing building energy use and location info
#
# OPTIONS: --
# AUTHOR: Data Gaudium
# CONTACT:
#==============================================================================
from __future__ import print_function, division, absolute_import
from six import iteritems
import tables
import os
import re
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
from scipy.spatial import ConvexHull
from geopy.distance import geodesic
import parse_csv_data as cvs_parsers

# default global file name locations
dir_path = os.path.dirname(os.path.relpath(__file__))
if dir_path:
    dir_path += "/"
long_lat_dflt = dir_path + "./SMC data challenge - Jibo/ChicagoLoopBuildings-Lat-Lon-height.csv"
mtr_base_dir_dflt = dir_path + "./SMC data challenge - Jibo/EnergyPlus-Buildings-Output-whole-year/final_h5_files/"
htm_base_dir_dflt = dir_path + "./SMC data challenge - Jibo/EnergyPlus-Buildings-Output-whole-year/htm_files/"


class BuildingGenerator(object):
    def __init__(self, long_lat_filename=long_lat_dflt, mtr_dir_base=mtr_base_dir_dflt, htm_base_dir=htm_base_dir_dflt):
        self.long_lat_dframe = cvs_parsers.read_long_lat_file(long_lat_filename)
        self.mtr_dir_base = mtr_dir_base
        self.htm_base_dir = htm_base_dir

    def gen_building(self, bld_id):
        if bld_id not in self.building_ids:
            raise KeyError("Invalid building ID")
        attr_dict = {}
        for attr in self.valid_attrs:
            attr_dict[attr] = self.get_building_attr(bld_id, attr)
        mtr_file = self.mtr_dir_base + "/" + str(int(bld_id)) + "_final.h5"
        htm_file = self.htm_base_dir + "/" + str(int(bld_id)) + "_final.htm"
        return Building(bld_id, mtr_file, htm_file, **attr_dict)

    def get_building_loc_data(self, bld_id):
        return self.long_lat_dframe.loc[self.long_lat_dframe["ID"] == bld_id]

    @property
    def building_ids(self):
        return np.array(self.long_lat_dframe["ID"])

    def get_building_centroids(self, bld_id, coords="m"):
        bld_loc_data = self.get_building_loc_data(bld_id)
        return bld_loc_data["Centroid"][0]

    def get_building_attr(self, bld_id, attr):
        self._validate_attr(attr)
        bld_loc_data = self.get_building_loc_data(bld_id)
        return bld_loc_data[attr]

    def _validate_attr(self, attr):
        if attr not in self.valid_attrs:
            raise KeyError

    @property
    def valid_attrs(self):
        return ['Centroid', 'Footprint2D', 'WWR_surfaces', 'ID', 'Height', 'NumFloors']


class Building(object):
    def __init__(self, bld_id, mtr_file, htm_file=None, **kwargs):
        # reference location
        self.id = bld_id
        self.lat0 = kwargs.get("lat0", 41.8)     # deg N from equator
        self.long0 = kwargs.get("long0", -87.6)  # deg from PM (negative is west)
        self._energy_data_interface = EnergyData(mtr_file)
        self.attr_dict = kwargs
        try:
            self._htm_attrs_dict = parse_htm(htm_file)
        except:
            print("WARNING: Invalid or missing HTM building file.")
            self._htm_attrs_dict = {}

    @property
    def conditioned_area(self):
        return [self._htm_attrs_dict["conditioned_area"] if "conditioned_area" in \
                self._htm_attrs_dict.keys() else np.nan][0]

    @property
    def conditioned_volume(self):
        return [self._htm_attrs_dict["conditioned_volume"] if "conditioned_volume" in \
                self._htm_attrs_dict.keys() else np.nan][0]

    @property
    def wall_surface_area(self):
        return [self._htm_attrs_dict["wall_surface_area"] if "wall_surface_area" in \
                self._htm_attrs_dict.keys() else np.nan][0]

    @property
    def window_area(self):
        return [self._htm_attrs_dict["window_area"] if "window_area" in \
                self._htm_attrs_dict.keys() else np.nan][0]

    @property
    def lighting_power_density(self):
        return [self._htm_attrs_dict["lighting_power_density"] if "lighting_power_density" in \
                self._htm_attrs_dict.keys() else np.nan][0]

    @property
    def plug_process_density(self):
        return [self._htm_attrs_dict["plug_process_density"] if "plug_process_density" in \
                self._htm_attrs_dict.keys() else np.nan][0]

    @property
    def people_density(self):
        return [self._htm_attrs_dict["people_density"] if "people_density" in \
                self._htm_attrs_dict.keys() else np.nan][0]

    @property
    def energy_interface(self):
        return self._energy_data_interface

    @energy_interface.setter
    def energy_interface(self, energy_interface):
        assert isinstance(energy_interface, TransientData)
        self._energy_data_interface = energy_interface

    @property
    def height(self):
        return float(self.attr_dict["Height"])

    @property
    def floors(self):
        return float(self.attr_dict["NumFloors"])

    @property
    def footprint(self):
        # returns a np array of tuples for (lat, long) verticies of the footprint
        return np.array(self.attr_dict["Footprint2D"].values[0])

    @property
    def perimeter(self):
        perim = 0
        for i in np.arange(len(self.footprint)) + 1:
            lat_long_a, lat_long_b = self.footprint[i - 1], self.footprint[i - 2]
            side_len = geodesic(lat_long_a, lat_long_b).meters
            perim += side_len
        return perim

    @property
    def footprint_area(self):
        xy_footprint_points = []
        for vertex in self.footprint:
            rel_x_coord = geodesic((self.ref_lat_long[0], vertex[1]), vertex).meters
            rel_y_coord = geodesic((vertex[0], self.ref_lat_long[1]), vertex).meters
            xy_footprint_points.append((rel_x_coord, rel_y_coord))
        return ConvexHull(xy_footprint_points).volume

    @property
    def volume(self):
        # WARNING: this value is inconsistent with the value in the *.htm files.
        return self.footprint_area * self.height

    @property
    def ref_lat_long(self):
        return np.array([self.lat0, self.long0])

    @property
    def exterior_surface_area(self):
        # WARNING: this value is inconsistent with the value in the *.htm files
        return self.perimeter * self.height

    @property
    def centroid(self):
        # returns a np array of (lat, long) pair
        return np.array(self.attr_dict["Centroid"].values[0][0])


def parse_htm(html_file):
    """!
    @brief Parses HTM files for building information.
    """
    htm_attr_dict = {"conditioned_volume": None, "conditioned_area": None, "wall_surface_area": None,
                     "window_area": None, "lighting_power_density": None, "people_density": None,
                     "plug_process_density": None}
    with open(html_file) as fp:
        soup = BeautifulSoup(fp, "lxml")
        # iterate over all tables in htm file
        for htm_table in soup.find_all("table", border=1):
            # iterate over rows in table
            for row in  htm_table.findAll(lambda tag: tag.name=='tr'):
                str_row = str(row)
                if re.findall(r"Conditioned Total", str_row):
                    # search rows
                    match_list = re.findall(r"[-+]?\d*\.*\d+", str_row)
                    if match_list:
                        htm_attr_dict["conditioned_area"] = float(match_list[0])
                        htm_attr_dict["conditioned_volume"] = float(match_list[1])
                        htm_attr_dict["wall_surface_area"] = float(match_list[2])
                        htm_attr_dict["window_area"] = float(match_list[4])
                        htm_attr_dict["lighting_power_density"] = float(match_list[6])
                        htm_attr_dict["people_density"] = float(match_list[7])
                        htm_attr_dict["plug_process_density"] = float(match_list[8])
                    else:
                        print("WARNING: Values Not found in table. Skipping.")
                        break
            if htm_attr_dict["wall_surface_area"] and htm_attr_dict["conditioned_area"]:
                break
    return htm_attr_dict


class TransientData(object):
    def __init__(self):
        pass

    def get_field(self, field_name, t0, tf, time_fmt):
        raise NotImplementedError

    def get_times(self, t0, tf, time_fmt):
        raise NotImplementedError

    @staticmethod
    def _slice_field(mtr_store, field_name, t0, tf):
        # for pandas store formated data
        return mtr_store['table'][field_name][(mtr_store['table']['Date/Time'] >= t0) & \
                                              (mtr_store['table']['Date/Time'] <= tf)]

    @staticmethod
    def _slice_field_pytables(table, field_name, t0, tf):
        # for pytables table format
        condition = '(Date/Time >= t0) & (Date/Time <= tf)'
        condition_vars = {'t0': t0, 'tf': tf}
        return [x[field_name] for x in table.where(condition, condition_vars)]


class EnergyData(TransientData):
    def __init__(self, mtr_h5_file, **kwargs):
        """!
        @brief Transient energy data interface.  Uses pytables to
        query transient data that matches certain dates or times
        @param kwargs
        """
        self.mtr_file = mtr_h5_file

    def get_field(self, field_name, t0='1/1/1980', tf='1/1/2100', time_fmt="datetime", out_fmt='np'):
        """!
        @brief find time series data that falls between t0 and tf.  By default
        get everything.
        """
        if "rel_humidity" in field_name:
            return self.get_rel_humidity(t0, tf)
        if "abs_humidity" in field_name:
            return self.get_abs_humidity(t0, tf)
        self._validate_attr(field_name)
        df_field_name = self.valid_attrs[field_name]
        t0 = cvs_parsers.datetime_to_int(pd.to_datetime(t0))
        tf = cvs_parsers.datetime_to_int(pd.to_datetime(tf))
        # open the mtr_h5 file for reading
        with pd.HDFStore(self.mtr_file) as mtr_store:
            # read desired info
            sliced_field = self._slice_field(mtr_store, df_field_name, t0, tf)
        if out_fmt == 'np':
            return sliced_field.values
        else:
            return sliced_field

    def get_rel_humidity(self, t0='1/1/1980', tf='1/1/2100'):
        """!
        @brief Compute and return relative humidity outside the building between t0 and tf.
        """
        wetbulb = self.get_field("site_wetbulb_t", t0, tf)
        drybulb = self.get_field("site_drybulb_t", t0, tf)
        rel_humidity = self.rel_humidity_from_wet_dry_temp(drybulb, wetbulb)
        return rel_humidity

    def get_abs_humidity(self, t0, tf):
        """!
        @brief Compute absolute humidity in [g/m^3]
        source:
        https://carnotcycle.wordpress.com/2012/08/04/how-to-convert-relative-humidity-to-absolute-humidity/
        """
        drybulb = self.get_field("site_drybulb_t", t0, tf)
        if np.max(drybulb[~np.isnan(drybulb)]) > 180:
            T = drybulb - 273.15
        else:
            T = drybulb
        # T in deg C
        rh = self.get_rel_humidity(t0, tf)
        abs_humidity = self.abs_humidity_from_rh(T, rh)
        return abs_humidity

    @staticmethod
    def abs_humidity_from_rh(T, rh):
        """"!
        @brief absolute humidity in [g/m^3]
        @param T temperature in [C]
        @param rh  rel humidity in (0., 1.0)
        """
        abs_humidity = (6.112 * np.exp((17.67 * T) / (T + 243.5)) * 100. * rh * 18.02) / \
                ((273.15 + T) * 100 * 0.08314)
        return abs_humidity

    def get_times(self, t0='1/1/1980', tf='1/1/2100', time_fmt="datetime",
                  date_time_str="date", out_fmt='int'):
        """!
        @brief Get a list of available times
        """
        int_times = self.get_field(date_time_str, t0, tf, time_fmt)
        if out_fmt == 'int':
            return int_times
        else:
            return np.array([cvs_parsers.int_to_datetime(time) for time in int_times])

    def get_time_deltas(self, t0='1/1/1900', tf='1/1/2100', time_fmt="datetime"):
        """!
        @brief Get a list of time step sizes.
        """
        times = self.get_times(t0, tf, time_fmt)
        dt = np.diff(times)
        return np.concatenate((dt, [dt[-1]]))

    def _validate_attr(self, attr):
        if attr not in self.valid_attrs.keys():
            print("Available keys are: %s" % (str(self.valid_attrs.keys())))
            raise KeyError

    @property
    def valid_attrs(self):
        return {"date": "Date/Time",
                "gas_timestep": "Gas:Facility [J](TimeStep)",
                "elec_timestep": "Electricity:Facility [J](TimeStep)",
                "site_drybulb_t": "Environment:Site Outdoor Air Drybulb Temperature [C](Hourly)",
                "temperature": "Environment:Site Outdoor Air Drybulb Temperature [C](Hourly)",
                "site_wetbulb_t": "Environment:Site Outdoor Air Wetbulb Temperature [C](Hourly)"}

    @staticmethod
    def rel_humidity_from_wet_dry_temp(drybulb, wetbulb):
        """!
        @brief Aprox computes relative humidity from wet and drybulb temperatures.
        source: http://www.1728.org/relhum.htm
        """
        # ensure temperatures in C
        if np.max(drybulb[~np.isnan(drybulb)]) > 180:
            drybulb_t = drybulb - 273.15
            wetbulb_t = wetbulb - 273.15
        else:
            drybulb_t = drybulb
            wetbulb_t = wetbulb
        e_d = 6.112 * np.exp((17.502 * drybulb_t) / (240.97 + drybulb_t))
        e_w = 6.112 * np.exp((17.502 * wetbulb_t) / (240.97 + wetbulb_t))
        N = 0.6687451584
        rel_humidity = (e_w - N * (1. + 0.00115 * wetbulb_t) * (drybulb_t - wetbulb_t)) / e_d
        # cannot have <0 or >1 rel humidity
        rel_humidity = np.clip(rel_humidity, 0.0, 1.0)
        return rel_humidity

if __name__ == "__main__":
    # example use
    bg = BuildingGenerator()
    all_avail_ids = bg.building_ids
    print("Available Buildings by ID: ")
    print(all_avail_ids)
    my_build_id = all_avail_ids[213]
    # make a building instance
    my_building = bg.gen_building(my_build_id)

    # ex attributes
    print("Building ID:")
    print(my_building.id)
    print("Centroid (lat, long): ")
    print(my_building.centroid)
    print("Footprint (lat, long) pairs: ")
    print(my_building.footprint)
    print("Footprint Area [m^2]: ")
    print(my_building.footprint_area)
    print("Perimeter [m]: ")
    print(my_building.perimeter)
    print("Height [m]: ")
    print(my_building.height)
    print("Volume [m^3]: ")
    print(my_building.volume)
    print("Exterior surface area [m^2]: ")
    print(my_building.wall_surface_area)
    print("Conditioned Volume [m^3]")
    print(my_building.conditioned_volume)
    print("Conditioned Area [m^2]")
    print(my_building.conditioned_area)
    print("Window Area [m^2]")
    print(my_building.window_area)
    print("Lighting Energy Density [W/m^2]")
    print(my_building.lighting_power_density)
    print("Plug power density [W/m^2]")
    print(my_building.plug_process_density)
    print("Personal space [m^2/n_people]")
    print(my_building.people_density)

    # ex gas and elec use vs time for one month
    t0, tf = '4/1/2017', '4/30/2017'
    times = my_building.energy_interface.get_times(t0, tf)
    time_deltas = my_building.energy_interface.get_time_deltas(t0, tf)
    gas = my_building.energy_interface.get_field("gas_timestep", t0, tf)
    elec = my_building.energy_interface.get_field("elec_timestep", t0, tf)
    temp = my_building.energy_interface.get_field("site_drybulb_t", t0, tf)
    t_wet = my_building.energy_interface.get_field("site_wetbulb_t", t0, tf)
    rel_humidity = my_building.energy_interface.get_field("rel_humidity", t0, tf)
    abs_humidity = my_building.energy_interface.get_field("abs_humidity", t0, tf)

    # data was recorded in 10 minute intervals
    delta_t_s = time_deltas

    # plot results
    import matplotlib.pyplot as plt
    import seaborn as sns
    with sns.axes_style("whitegrid", {"grid.linestyle": '--', "grid.alpha": 0.6}):
        plt.figure()
        plt.scatter((times - np.min(times)) / 60. / 60. / 24., gas / delta_t_s / 1000., label="Gas use [kW]", s=3)
        plt.legend()
        plt.title(r"Bld ID=%s,  $t_0$= %s,  $t_f$= %s" % (str(my_building.id), str(t0), str(tf)))
        plt.xlabel("time [days]")
        plt.ylabel("Energy Use [kW]")
        plt.savefig("ex_gas.png")
        plt.close()
        plt.figure()
        plt.scatter((times - np.min(times)) / 60. / 60. / 24., elec / delta_t_s / 1000., label="Elec use [kW]", s=3)
        plt.legend()
        plt.title(r"Bld ID=%s,  $t_0$= %s,  $t_f$= %s" % (str(my_building.id), str(t0), str(tf)))
        plt.ylabel("Energy Use [kW]")
        plt.xlabel("time [days]")
        plt.savefig("ex_elec.png")
        plt.close()
        plt.figure()
        plt.scatter((times - np.min(times)) / 60. / 60. / 24., temp, label="Temperature [C]", s=3)
        plt.legend()
        plt.title(r"Bld ID=%s,  $t_0$= %s,  $t_f$= %s" % (str(my_building.id), str(t0), str(tf)))
        plt.ylabel("Outdoor drybulb temp [C]")
        plt.xlabel("time [days]")
        plt.savefig("ex_temp_dry.png")
        plt.close()
        plt.figure()
        plt.scatter((times - np.min(times)) / 60. / 60. / 24., t_wet, label="Temperature [C]", s=3)
        plt.legend()
        plt.title(r"Bld ID=%s,  $t_0$= %s,  $t_f$= %s" % (str(my_building.id), str(t0), str(tf)))
        plt.ylabel("Outdoor wetbulb temp [C]")
        plt.xlabel("time [days]")
        plt.savefig("ex_temp_wet.png")
        plt.close()

        # Outdoor humidity as fn of time
        plt.figure()
        plt.scatter((times - np.min(times)) / 60. / 60. / 24., rel_humidity * 100, label="Relative Humidty [%]", s=3)
        plt.legend()
        plt.title(r"Bld ID=%s,  $t_0$= %s,  $t_f$= %s" % (str(my_building.id), str(t0), str(tf)))
        plt.ylabel("Outdoor rel humidity [%]")
        plt.xlabel("time [days]")
        plt.savefig("ex_humid_rel.png")
        plt.close()
        plt.figure()
        plt.scatter((times - np.min(times)) / 60. / 60. / 24., abs_humidity, label="Abs Humidty [g/m^3]", s=3)
        plt.legend()
        plt.title(r"Bld ID=%s,  $t_0$= %s,  $t_f$= %s" % (str(my_building.id), str(t0), str(tf)))
        plt.ylabel("Outdoor abs humidity [g/m^3]")
        plt.xlabel("time [days]")
        plt.savefig("ex_humid_abs.png")
        plt.close()

        # plot T and elec on same plot
        fig, ax1 = plt.subplots()
        # share x axis
        ax1.plot((times - np.min(times)) / 60. / 60. / 24., elec / delta_t_s / 1000., label="Elec use [kW]")
        ax1.set_ylabel("Energy Use [kW]")
        ax1.set_xlabel("time [days]")
        ax2 = ax1.twinx()
        ax2.plot((times - np.min(times))[~np.isnan(temp)] / 60. / 60. / 24., temp[~np.isnan(temp)], label="Temperature [C]", c='r')
        ax2.set_ylabel("Outdoor drybulb temp [C]")
        plt.title(r"Bld ID=%s,  $t_0$= %s,  $t_f$= %s" % (str(my_building.id), str(t0), str(tf)))
        plt.legend()
        plt.savefig("ex_temp_dry.png")
        plt.close()
