#!/bin/bash
# Wgets the data and unzips all the data and places it in a folder

# get the data if we dont already have it
if [ ! ./UrbanChallengeDataSet.zip ]; then
    wget https://smcdatachallenge.ornl.gov/SMC18_ChallengeData/Challenge2_DataSet/UrbanChallengeDataSet.zip
fi

output_dir=SMC\\ data\\ challenge\\ -\\ Jibo/EnergyPlus-Buildings-Output-whole-year

# unzip the file
if [ ! -d "$output_dir" ]; then
    unzip UrbanChallengeDataSet.zip
fi

# cd into unzip dir
cd $output_dir

# Unzip the node files
for i in {1..20} ; do
    mkdir node_out_$i && mkdir tar zxvf node_$i.tar.gz -C node_out_$i
done

# gather all final.csv files together
mkdir -p final_files
for i in {1..20} ; do
    cp node_out_$i/*_final.csv final_files/.
done

# gather all final.htm files together
mkdir -p htm_files
for i in {1..20} ; do
    cp node_out_$i/*_final.htm htm_files/.
done

# prep h5 dir
mkdir -p final_h5_files

cd ../..

# run python csv parser script
echo "Running csv to hdf5 conversion. This might take 1hr."
python parse_csv_data.py --final
