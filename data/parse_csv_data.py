#!/usr/bin/env python
#-*- coding: utf-8 -*-
#==============================================================================
# Copyright (c) 2018, The Members of Data Gaudium
# All rights reserved.
#
# DESCRIPTION:
# Parses csv files
#
# OPTIONS: --
# AUTHOR: Data Gaudium
# CONTACT:
#==============================================================================
from __future__ import print_function, division, absolute_import
from six import iteritems
from pathlib import Path
import tables
import pandas as pd
import numpy as np
import argparse
import os
import re

#  str converters
def long_lat_conv(long_lat_str):
    """!
    @brief Converts the longitude latitude strings into numpy vecs
    @return list of np arrays
    """
    clean_long_lat_str = long_lat_str.replace('/', '_')
    long_lat_vec = np.fromstring(clean_long_lat_str, sep='_')
    # split vec into list of tuples
    long_lat_vec = np.split(long_lat_vec, int(len(long_lat_vec) / 2))
    return long_lat_vec

def wwr_conv(wwr_str):
    """!
    @brief Converts the wwr string to a np_1darray
    @return np_1darray
    """
    wwr_vec = np.fromstring(wwr_str, sep='_')
    return wwr_vec

def read_long_lat_file(csv_file, index_col=None):
    """!
    @brief Converts long_lat_height file into a pandas dataframe
    @param csv_file str file string
    @param index_col int  if 0, use first colum as table row indexes.
        Default: use row index as table indicies.
        If index_col==0:  Uses Building ID as table indicies.
    @return pandas dataFrame containing longitude, latitude and building heights
    """
    long_lat_dframe = \
        pd.read_csv(csv_file,
                    index_col=index_col,
                    header=0,
                    parse_dates=False,
                    converters={'Centroid': long_lat_conv,
                                'Footprint2D': long_lat_conv,
                                'WWR_surfaces': wwr_conv})
    return long_lat_dframe


def read_mtr_file(mtr_csv_file, datetime_fmt='int'):
    """!
    @brief Parses a mtr csv file into a pandas dataframe.
    @param mtr_csv_file str file string
    @return (pandas.dataFrame, int) tuple
    """
    mtr_dframe = \
        pd.read_csv(mtr_csv_file,
                    header=0,
                    parse_dates=True)
    building_id = None
    # try to get building id from csv filename
    matches = re.match(".*\/(\d+)_", mtr_csv_file)
    if matches:
        building_id = int(matches.group(1))
    mtr_dframe.rename(columns=lambda c_name: c_name.strip(), inplace=True)
    if datetime_fmt == 'int':
        print("Cleaning date/time...")
        mtr_dframe = conv_times_to_ints(mtr_dframe)
    return mtr_dframe, building_id


def conv_times_to_ints(dframe, time_col="Date/Time"):
    dframe[time_col] = dframe[time_col].apply(lambda t: datetime_to_int(t))
    return dframe


def int_to_datetime(int_time, epoch='unix'):
    """!
    @brief converts integer time to pandas datetime format.
    """
    return pd.to_datetime(int_time, unit='s', origin=epoch)


def datetime_to_int(datetime_time, epoch='unix', year_str="2017"):
    """!
    @brief converts pandas datetime to integer.
    """
    try:
        datetime_time = pd.to_datetime(datetime_time)
    except:
        try:
            datetime_time = pd.to_datetime(year_str + "/" + str(datetime_time))
        except:
            datetime_time = pd.to_datetime(year_str + "/" + re.sub(r" 24:", " 00:", str(datetime_time))) + pd.DateOffset(1)
    df = pd.DataFrame.from_dict({'date': {0: datetime_time}})
    return pd.to_timedelta(df.date).dt.total_seconds().astype(int)[0]


def date_to_int(time):
    if isinstance(time, int) or isinstance(time, float):
        return datetime_to_int(pd.to_datetime(time, unit='s'))
    else:
        return datetime_to_int(pd.to_datetime(time))


def write_dframe_to_h5(dframe, output_file):
    """!
    @brief store as pytables table
    """
    dframe.to_hdf(output_file, 'table', complevel=7)


def read_from_h5(infile):
    """!
    @brief returns a pytables File object
    """
    # return tables.open_file(infile, mode='r')
    h5store = pd.HDFStore(infile)
    return h5store


def test():
    # ex load the long_lat file
    long_lat_file = "./SMC data challenge - Jibo/ChicagoLoopBuildings-Lat-Lon-height.csv"
    long_lat_dframe = read_long_lat_file(long_lat_file)
    print(long_lat_dframe.loc[0, :])

    # ex load an mtr file
    mtr_file = "./SMC data challenge - Jibo/EnergyPlus-Buildings-Output-whole-year/mtr_files/100_final_mtr.csv"
    mtr_dframe, building_id = read_mtr_file(mtr_file)

    # print one row of mtr data
    print("=== Building ID: %s ===" % (str(building_id)))
    print(mtr_dframe.loc[0, :])

    # get row from the table matching building_id
    print(long_lat_dframe.loc[long_lat_dframe["ID"] == building_id])

    # rows of [data/time, Gas:Facility [J](TimeStep)] data
    mtr_times = mtr_dframe["Date/Time"]
    mtr_gas_timestep = mtr_dframe["Gas:Facility [J](TimeStep)"]
    mtr_elec_timestep = mtr_dframe["Electricity:Facility [J](TimeStep)"]


def main(convert_mtr=False, convert_final=False):
    # convert csv to h5
    if convert_mtr:
        mtr_base_dir = \
                "./SMC data challenge - Jibo/EnergyPlus-Buildings-Output-whole-year/mtr_files/"
        pathlist = Path(mtr_base_dir).glob('**/*.csv')
        for path in pathlist:
            path_in_str = str(path)
            print("Converting csv file: %s into H5 file." % (path_in_str))
            mtr_dframe, building_id = read_mtr_file(path_in_str)
            write_dframe_to_h5(mtr_dframe, mtr_base_dir + '../mtr_h5_files/' + str(building_id) + '_final_mtr.h5')

    if convert_final:
        mtr_base_dir = \
                "./SMC data challenge - Jibo/EnergyPlus-Buildings-Output-whole-year/final_files/"
        pathlist = Path(mtr_base_dir).glob('**/*.csv')
        for path in pathlist:
            path_in_str = str(path)
            print("Converting csv file: %s into H5 file." % (path_in_str))
            mtr_dframe, building_id = read_mtr_file(path_in_str)
            write_dframe_to_h5(mtr_dframe, mtr_base_dir + '../final_h5_files/' + str(building_id) + '_final.h5')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Utility to convert csv files to hdf5.")
    parser.add_argument("--mtr", action='store_true')
    parser.add_argument("--final", action='store_true')
    args = parser.parse_args()
    convert_mtr = False
    convert_final = False
    if args.mtr:
        convert_mtr = True
    if args.final:
        convert_final = True
    main(convert_mtr, convert_final)
