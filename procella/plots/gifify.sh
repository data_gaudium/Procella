#!/bin/bash

# list files to use in gif
in_files=()
for i in {1..12}; do
    in_files+=(../month_"$i"_figs/city_elec_use_scatter_matrix.png)
done
echo "${in_files[@]}"

gif_out="month_by_month.gif"

# generate the gif
convert -loop 0 -delay 100 ${in_files[@]} $gif_out
