#!/usr/bin/env python
#-*- coding: utf-8 -*-
#==============================================================================
# Copyright (c) 2018, The Members of Data Gaudium
# All rights reserved.
#
# DESCRIPTION:
# Creates histograms and scatterplots of building energy usage
#
# OPTIONS: --
# AUTHOR: Data Gaudium
# CONTACT:
#==============================================================================
from __future__ import print_function, division, absolute_import
from six import iteritems
import matplotlib.pyplot as plt
import tables
import argparse
import pandas as pd
import numpy as np
import sys, os
# data dir to pythonpath
sys.path.append(os.path.join(sys.path[0], '../data'))
from building_data_interface import BuildingGenerator
import parse_csv_data as pcd
from plots import mv_plot
# gen holiday filter
from pandas.tseries.holiday import USFederalHolidayCalendar as holiday_cal
dr = pd.date_range(start='2017-01-01', end='2017-12-31')
cal = holiday_cal()
# global constant pre-computed holiday list
HOLIDAYS = cal.holidays(start=dr.min(), end=dr.max())


def field_average(field_vals, time_deltas=None):
    """
    @brief Compute the time-averaged field values.
    @param field_vals np_1d array
    @param time_deltas np_1d array of time deltas in s
        Default: Assumes all time step sizes are equal
        and therefore have equal contribution to the average.
    """
    if field_vals.size == 0:
        return np.nan
    valid_locs = ~np.isnan(field_vals)
    if time_deltas is None:
        return np.average(field_vals[valid_locs])
    else:
        return np.average(field_vals[valid_locs], weights=time_deltas[valid_locs])


def field_sum(field_vals):
    """
    @brief Compute the sum of a field
    @param field_vals np_1d array
    @return float
    """
    if field_vals.size == 0:
        return np.nan
    valid_locs = ~np.isnan(field_vals)
    return np.sum(field_vals[valid_locs])


def weekend_filter(times):
    """!
    @brief Filters times corropsonding to weekends.
    @return np_1d array of bool. True if value is weekday.
    """
    weekday_bools = np.ones(times.size, dtype=bool)
    for i, datetime in enumerate(gen_datetimes(times)):
        weekday_bools[i] = (datetime.dayofweek < 5)
    return weekday_bools


def midnight_filter(times, hr_0=23, hr_f=0):
    """!
    @brief Filter data from specific hourly window range.
    @return np_1d array of bool. True if value is not between hr_0 and hr_f
    """
    non_midnight = np.ones(times.size, dtype=bool)
    for i, datetime in enumerate(gen_datetimes(times)):
        non_midnight[i] = not ((datetime.hour >= hr_0) | (datetime.hour <= hr_f))
    return non_midnight


def holiday_filter(times):
    """!
    @brief Used to filter out holidays
    @return np_1d array of bool. True if value is not a holiday.
    """
    non_holiday_bools = np.ones(times.size, dtype=bool)
    for i, datetime in enumerate(gen_datetimes(times)):
        non_holiday_bools[i] = not (datetime in HOLIDAYS)
    return non_holiday_bools


def gen_datetimes(times):
    """!
    @brief Helper function converting ints to pandas datetimes
    Note: Ints must be in std unix format with std unix reference epoch.
    """
    for time in times:
        assert isinstance(time, int)
        datetime = pcd.int_to_datetime(time)
        yield datetime


def generate_time_windows(building, field_name, t0='4/1/2017', tf='4/20/2017', dt=1.0,
                          dt_fmt='day', weekend_cut=False, invert_cut=False):
    """!
    @brief Generates day, hour, month, or seasonal time windows
    that can be used to select the desired data from the building
    data interface.  Can also remove weekend values to only extract
    business day values.
    @param t0 str start time/date
    @param dt int. time window size
    @param dt_fmt time delta units ['hr', 's', 'day']
    @param weekend_cut bool.  If true, cuts weekend vals
    """
    if dt_fmt == 'day':
        window_size_s = dt * 24. * 60. * 60.
    elif dt_fmt == 'hr':
        window_size_s = dt * 60. * 60.
    elif dt_fmt == 's':
        window_size_s = dt
    times = building.energy_interface.get_times(t0, tf)
    field = building.energy_interface.get_field(field_name, t0, tf)
    # filter out weekend times
    if weekend_cut:
        weekdays = weekend_filter(times)
        non_holidays = holiday_filter(times)
        non_midnight = midnight_filter(times)
        if invert_cut:
            # only select weekends
            time_filter = ~weekdays * non_holidays * non_midnight
        else:
            time_filter = weekdays * non_holidays * non_midnight
        times = times[time_filter]
        field = field[time_filter]
    t0_s = times[0]
    time_breaks = []
    for i, time in enumerate(times):
        delta_t = time - t0_s
        if delta_t > window_size_s:
            t0_s = time
            time_breaks.append(i)
    split_times = np.split(times, time_breaks)
    split_field = np.split(field, time_breaks)
    return split_times, split_field


def get_time_averaged_fields(building, field_name, time_windows, **kwargs):
    split_fields = []
    split_times = []
    for time_win in time_windows:
        s_times, split_field = generate_time_windows(building, field_name, *time_win, **kwargs)
        split_times.append(s_times)
        split_fields.append(split_field)
    return split_times, split_fields


def building_stats(my_build_id, time_windows, invert_cut=False, output_prefix='./figs/'):
    # make a building instance
    my_building = bg.gen_building(my_build_id)

    split_field_dict = {'site_drybulb_t': None, 'elec_timestep': None, 'gas_timestep': None, 'abs_humidity': None}
    for field_name in split_field_dict.keys():
        times, split_fields = get_time_averaged_fields(my_building, field_name, time_windows,
                                                       invert_cut=invert_cut)
        split_field_dict['times'] =  times
        split_field_dict[field_name] = split_fields

    # Compute averages
    avg_elec, avg_gas, avg_t_dry, avg_humid = [], [], [], []
    for elec_use, gas_use, t_dry_outisde, abs_humidity in zip(np.concatenate(split_field_dict['elec_timestep']),
                                                              np.concatenate(split_field_dict['gas_timestep']),
                                                              np.concatenate(split_field_dict['site_drybulb_t']),
                                                              np.concatenate(split_field_dict['abs_humidity'])):
        # append to time_averaged arrays
        avg_elec.append(field_average(elec_use))
        avg_gas.append(field_average(gas_use))
        avg_t_dry.append(field_average(t_dry_outisde))
        avg_humid.append(field_average(abs_humidity))

    # norm factor conversion from J to W
    # (J sampled at 10 min intervals so 10 * 60 s pass each tstep)
    power_norm = 10. * 60

    # get building params to use for power consuption normalization
    c_area = my_building.conditioned_area
    w_area = my_building.wall_surface_area
    b_vol = my_building.conditioned_volume

    # normalize by conditioned_area and plot histogram
    avg_elec = np.array(avg_elec) / c_area / power_norm
    avg_gas = np.array(avg_gas) / c_area / power_norm
    avg_t_dry = np.array(avg_t_dry)
    avg_humid = np.array(avg_humid)

    # plot scatterplot/histograms
    d_frame_elec = pd.DataFrame.from_dict({"Day-Avg Drybulb T [C]": avg_t_dry,
                                           "Day-Avg Elec Use per Conditioned Area [W/m^2]": avg_elec,})
    d_frame_gas = pd.DataFrame.from_dict({"Day-Avg Drybulb T [C]": avg_t_dry,
                                          "Day-Avg Gas Use per Conditioned Area [W/m^2]": avg_gas})
    mv_plot.matrixPairPlot(d_frame_elec, savefig=output_prefix + str(my_build_id) + "_elec_use_scatter_matrix.png",
            title="Building ID:" + str(my_build_id), building_id=my_build_id,
            xy_file=output_prefix + "xy_file_elec.log",
            mean_file=output_prefix + "mean_file_elec.log")
    plt.close()
    mv_plot.matrixPairPlot(d_frame_gas, savefig=output_prefix + str(my_build_id) + "_gas_use_scatter_matrix.png",
            title="Building ID:" + str(my_build_id), building_id=my_build_id,
            xy_file=output_prefix + "xy_file_gas.log",
            mean_file=output_prefix + "mean_file_gas.log")
    plt.close()
    return avg_elec, avg_gas, avg_t_dry, avg_humid


def summer_study():
    # === SUMMER Building Population Stat Analysis ===
    summer_output_prefix = './summer_figs/'
    weekend_cut = True
    invert_cut = False
    summer_windows = [# ('4/1/2017', '4/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    # ('5/1/2017', '5/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('6/1/2017', '6/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('7/1/2017', '7/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('8/1/2017', '8/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('9/1/2017', '9/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                   ]

    city_time_averaged_data_dict = {'avg_gas': [], 'avg_elec': [], 'avg_t_dry': []}
    for my_build_id in all_avail_ids:
        print("Processing building ID=%d ..." % my_build_id)
        try:
            bld_avg_elec, bld_avg_gas, bld_avg_t_dry, _h = \
                    building_stats(my_build_id, summer_windows,
                                   invert_cut=invert_cut,
                                   output_prefix=summer_output_prefix)
            city_time_averaged_data_dict['avg_gas'].append(bld_avg_gas)
            city_time_averaged_data_dict['avg_elec'].append(bld_avg_elec)
            city_time_averaged_data_dict['avg_t_dry'].append(bld_avg_t_dry)
        except:
            # guess this building has bad data... skip yolo
            pass

    # concat all building data together
    d_frame = pd.DataFrame.from_dict({
            "Day-Avg Drybulb T [C]": np.concatenate(city_time_averaged_data_dict['avg_t_dry']),
            "Day-Avg Elec Use per Conditioned Area [W/m^2]": np.concatenate(city_time_averaged_data_dict['avg_elec']),
            })
    d_frame_gas = pd.DataFrame.from_dict({
            "Day-Avg Drybulb T [C]": np.concatenate(city_time_averaged_data_dict['avg_t_dry']),
            "Day-Avg Gas Use per Conditioned Area [W/m^2]": np.concatenate(city_time_averaged_data_dict['avg_gas']),
            })
    mv_plot.matrixPairPlot(d_frame, savefig=summer_output_prefix + "/city_elec_use_scatter_matrix.png",
                           title="City Wide", building_id="City")
    mv_plot.matrixPairPlot(d_frame_gas, savefig=summer_output_prefix + "/city_gas_use_scatter_matrix.png",
                           title="City Wide", building_id="City")


def winter_study():
    # === Winter Building Population Stat Analysis ===
    winter_output_prefix = './winter_figs/'
    weekend_cut = True
    invert_cut = False
    winter_windows = [
                    ('12/1/2017', '12/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('1/1/2017', '1/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('2/1/2017', '2/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('3/1/2017', '3/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ]

    city_time_averaged_data_dict = {'avg_gas': [], 'avg_elec': [], 'avg_t_dry': []}
    for my_build_id in all_avail_ids:
        print("Processing building ID=%d ..." % my_build_id)
        try:
            bld_avg_elec, bld_avg_gas, bld_avg_t_dry, _h = \
                    building_stats(my_build_id, winter_windows,
                                   invert_cut=invert_cut,
                                   output_prefix=winter_output_prefix)
            city_time_averaged_data_dict['avg_gas'].append(bld_avg_gas)
            city_time_averaged_data_dict['avg_elec'].append(bld_avg_elec)
            city_time_averaged_data_dict['avg_t_dry'].append(bld_avg_t_dry)
        except:
            # guess this building has bad data... skip yolo
            pass

    # concat all building data together
    d_frame_elec = pd.DataFrame.from_dict({
            "Day-Avg Drybulb T [C]": np.concatenate(city_time_averaged_data_dict['avg_t_dry']),
            "Day-Avg Elec Use per Conditioned Area [W/m^2]": np.concatenate(city_time_averaged_data_dict['avg_elec']),
            })
    d_frame_gas = pd.DataFrame.from_dict({
            "Day-Avg Drybulb T [C]": np.concatenate(city_time_averaged_data_dict['avg_t_dry']),
            "Day-Avg Gas Use per Conditioned Area [W/m^2]": np.concatenate(city_time_averaged_data_dict['avg_gas']),
            })
    mv_plot.matrixPairPlot(d_frame_elec, savefig=winter_output_prefix + "/city_elec_use_scatter_matrix.png",
                           title="City Wide", building_id="City")
    mv_plot.matrixPairPlot(d_frame_gas, savefig=winter_output_prefix + "/city_gas_use_scatter_matrix.png",
                           title="City Wide", building_id="City")


def month_study(month_i):
    # === Month-to-Month study
    weekend_cut = True
    invert_cut = False

    month_str = str(int(month_i))
    month_output_prefix = './month_' + month_str + '_figs/'
    time_windows = [(month_str + '/1/2017', month_str + '/17/2017 00:00:00', 1.0, 'day', weekend_cut),]

    city_time_averaged_data_dict = {'avg_gas': [], 'avg_elec': [], 'avg_t_dry': [], 'abs_humidity': []}
    for my_build_id in all_avail_ids:
        print("Processing building ID=%d ..." % my_build_id)
        try:
            bld_avg_elec, bld_avg_gas, bld_avg_t_dry, bld_avg_humid = \
                    building_stats(my_build_id, time_windows,
                                   invert_cut=invert_cut,
                                   output_prefix=month_output_prefix)
            city_time_averaged_data_dict['avg_gas'].append(bld_avg_gas)
            city_time_averaged_data_dict['avg_elec'].append(bld_avg_elec)
            city_time_averaged_data_dict['avg_t_dry'].append(bld_avg_t_dry)
            city_time_averaged_data_dict['abs_humidity'].append(bld_avg_humid)
        except:
            # guess this building has bad data... skip yolo
            pass

    # make month-to-month plots
    d_frame_elec = pd.DataFrame.from_dict({
            "Day-Avg Drybulb T [C]": np.concatenate(city_time_averaged_data_dict['avg_t_dry']),
            "Day-Avg Humidity [g/m^3]": np.concatenate(city_time_averaged_data_dict['abs_humidity']),
            "Day-Avg Elec Use per Conditioned Area [W/m^2]": np.concatenate(city_time_averaged_data_dict['avg_elec']),
            })
    d_frame_gas = pd.DataFrame.from_dict({
            "Day-Avg Drybulb T [C]": np.concatenate(city_time_averaged_data_dict['avg_t_dry']),
            "Day-Avg Humidity [g/m^3]": np.concatenate(city_time_averaged_data_dict['abs_humidity']),
            "Day-Avg Gas Use per Conditioned Area [W/m^2]": np.concatenate(city_time_averaged_data_dict['avg_gas']),
            })
    mv_plot.matrixPairPlot(d_frame_elec, savefig=month_output_prefix + "/city_elec_use_scatter_matrix.png",
                           title="City Wide", building_id="City")
    mv_plot.matrixPairPlot(d_frame_gas, savefig=month_output_prefix + "/city_gas_use_scatter_matrix.png",
                           title="City Wide", building_id="City")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Utility to convert csv files to hdf5.")
    parser.add_argument("--month", type=int)
    parser.add_argument("--season", type=str)
    args = parser.parse_args()

    bg = BuildingGenerator()
    all_avail_ids = bg.building_ids
    print("Available Buildings by ID: ")
    print(all_avail_ids)
    # my_build_id = all_avail_ids[213]
    # all_avail_ids = all_avail_ids[[1,2]]

    if args.month:
        assert 1 <= int(args.month) <= 12
        month_study(args.month)

    if args.season:
        if args.season == 'summer':
            summer_study()
        if args.season == 'winter':
            winter_study()

