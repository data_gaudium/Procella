#!/usr/bin/env python
#-*- coding: utf-8 -*-
#==============================================================================
# Copyright (c) 2018, The Members of Data Gaudium
# All rights reserved.
#
# DESCRIPTION:
# Tallies energy use vs temperture slopes into histogram.  Rank-order
# buildings by slope.
#
# OPTIONS: --
# AUTHOR: Data Gaudium
# CONTACT:
#==============================================================================
from __future__ import print_function, division, absolute_import
from six import iteritems
import matplotlib.pyplot as plt
import seaborn as sns
import tables
import argparse
import pandas as pd
import numpy as np
import sys, os
import datetime
# data dir to pythonpath
sys.path.append(os.path.join(sys.path[0], '../data'))
from building_data_interface import BuildingGenerator
import parse_csv_data as pcd


def get_month_slope_data(prefix='elec'):
    month_avg_slopes = []
    for i in range(12):
        month_str = str(i + 1)
        xy_data = np.loadtxt('./month_' + month_str +'_figs/xy_file_' + prefix + '.log', delimiter=',')
        building_ids = xy_data[:, 0]
        slopes = xy_data[:, 1]
        sd_slopes = xy_data[:, 2]
        slopes_r2 = xy_data[:, 4]

        # compute average slope for the month
        month_avg_slopes.append(np.average(slopes))
    return month_avg_slopes

def month_slope_study(prefix='elec'):
    month_s = [str(i) + '/2017' for i in np.arange(1, 13, dtype=int)]
    month_avg_elec_slopes = get_month_slope_data('elec')
    month_avg_gas_slopes = get_month_slope_data('gas')

    # plot mont-avg slopes
    with sns.axes_style("whitegrid", {"grid.linestyle": '--', "grid.alpha": 0.6}):
        sns.set_palette(sns.diverging_palette(250, 15, s=70, l=45, n=3, center="light"))
        sns.set_color_codes()
        fig = plt.figure()
        ax = fig.add_subplot(111)
        plt.xticks(rotation=45)
        plt.xlabel("Month")

        from matplotlib.dates import MonthLocator, WeekdayLocator, DateFormatter
        ax.plot(month_s, month_avg_elec_slopes, label="Electric")
        ax.scatter(month_s, month_avg_elec_slopes)
        ax.axhline(0, c='k', alpha=0.7)
        ax.set_ylabel("Month Averaged"
                      " Electricity \n"
                      r"Use Sensitivity $\frac{\partial (EUI)}{\partial T} [W/m^2]/[K]$")

        ax2 = ax.twinx()
        ax2.plot(month_s, month_avg_gas_slopes, c='r', label="Gas")
        ax2.scatter(month_s, month_avg_gas_slopes, c='r')
        ax2.set_ylabel("Month Averaged"
                       " Gas \n"
                       r"Use Sensitivity $\frac{\partial (EUI)}{\partial T} [W/m^2]/[K]$")

        plt.tight_layout()
        ax2.legend(loc=0)
        ax.legend(loc=4)
        plt.savefig("month_avg_slopes.png")
        plt.close()


def hist_slope_xy(infile, prefix='elec'):
    xy_data = np.loadtxt(infile, delimiter=',')
    building_ids = xy_data[:, 0]
    slopes = xy_data[:, 1]
    sd_slopes = xy_data[:, 2]
    slopes_r2 = xy_data[:, 4]


    # sort by slope
    slope_sort_idx = np.argsort(slopes)
    slopes = slopes[slope_sort_idx][::-1]
    sd_slopes = sd_slopes[slope_sort_idx][::-1]
    building_ids = building_ids[slope_sort_idx][::-1]
    slopes_r2 = slopes_r2[slope_sort_idx][::-1]

    # positive slp mask
    pos_slp_mask = slopes > 0
    slopes = slopes[pos_slp_mask]
    sd_slopes = sd_slopes[pos_slp_mask]
    building_ids = building_ids[pos_slp_mask]
    slopes_r2 = slopes_r2[pos_slp_mask]

    # plot
    with sns.axes_style("whitegrid", {"grid.linestyle": '--', "grid.alpha": 0.6}):
        sns.set_palette(sns.diverging_palette(250, 15, s=70, l=45, n=3, center="light"))
        sns.set_color_codes()
        fig = plt.figure()
        ax = fig.add_subplot(111)
        sns.distplot(slopes, rug=True, ax=ax)
        ax.set_xlabel(r"Energy Use Sensitivity $\frac{\partial (EUI)}{\partial T} [W/m^2]/[K]$")
        ax.set_ylabel("Probability Density")

        # annotate upper and lower building ID
        ax.annotate(r"Max $\frac{\partial E}{\partial T}$: %0.2e "
                    "\n"
                    "Bld ID: %d" % (slopes[0], building_ids[0]),
                    xy=(0.87, 0.08), xytext=(0.87, 0.55), xycoords=ax.transAxes,
                    arrowprops=dict(facecolor='black', shrink=0.05),)
        ax.annotate(r"Min $\frac{\partial E}{\partial T}$: %0.2e "
                    "\n"
                    "Bld ID: %d" % (slopes[-1], building_ids[-1]),
                    xy=(0.13, 0.08), xytext=(0.13, 0.55), xycoords=ax.transAxes,
                    arrowprops=dict(facecolor='black', shrink=0.05),)

        # save fig
        fig.savefig(prefix + "_slope_histogram.png")
        plt.close()

    # create table of top 10 slopes
    print("Build ID, d[EUI]/d[T] [W/m^2]/[K], \sigma , R^2")
    for i in range(10):
        print(str(building_ids[i]) + ", " + str(slopes[i]) + ", " + str(sd_slopes[i]) + ", " + str(slopes_r2[i]))


if __name__ == "__main__":
    # hist_slope_xy('summer_figs/xy_file_elec.log')
    month_slope_study()


