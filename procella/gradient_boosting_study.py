#!/usr/bin/env python
#-*- coding: utf-8 -*-
#==============================================================================
# Copyright (c) 2018, The Members of Data Gaudium
# All rights reserved.
#
# DESCRIPTION:
# Compute relative influence of several building parameters and external conditions
# on energy use of a building using gradient boosting.
#
# OPTIONS: --
# AUTHOR: Data Gaudium
# CONTACT:
#==============================================================================
from __future__ import print_function, division, absolute_import
from six import iteritems
import matplotlib.pyplot as plt
import seaborn as sns
from copy import deepcopy
from matplotlib.ticker import FormatStrFormatter
import tables
import pandas as pd
import numpy as np
from sklearn import ensemble
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import train_test_split
from sklearn.ensemble.partial_dependence import plot_partial_dependence
import sys, os
sys.path.append(os.path.join(sys.path[0], '../data'))
from building_data_interface import BuildingGenerator, EnergyData
from frequency_study import *


def get_building_training_data(my_build_id, time_windows):
    # make a building instance
    my_building = bg.gen_building(my_build_id)

    split_field_dict = {'site_drybulb_t': None, 'elec_timestep': None, 'gas_timestep': None, 'abs_humidity': None}
    for field_name in split_field_dict.keys():
        times, split_fields = get_time_averaged_fields(my_building, field_name, time_windows,
                                                       invert_cut=False)
        split_field_dict['times'] =  times
        split_field_dict[field_name] = split_fields

    # Compute averages
    avg_elec, avg_gas, avg_t_dry, avg_humidity = [], [], [], []
    for elec_use, gas_use, t_dry_outisde, humidity in zip(np.concatenate(split_field_dict['elec_timestep']),
                                                          np.concatenate(split_field_dict['gas_timestep']),
                                                          np.concatenate(split_field_dict['site_drybulb_t']),
                                                          np.concatenate(split_field_dict['abs_humidity'])):
        # append to time_averaged arrays
        avg_elec.append(field_average(elec_use))
        avg_gas.append(field_average(gas_use))
        avg_t_dry.append(field_average(t_dry_outisde))
        avg_humidity.append(field_average(humidity))

    # norm factor conversion from J to W
    # (J sampled at 10 min intervals so 10 * 60 s pass each tstep)
    power_norm = 10. * 60

    # get building params to use for power consuption normalization
    c_area = my_building.conditioned_area

    # get building attributes
    b_vol = my_building.conditioned_volume
    b_floors = my_building.floors
    b_height = my_building.height
    b_win_area = my_building.window_area
    b_wall_area = my_building.wall_surface_area
    b_cetroid = my_building.centroid

    # weather data
    avg_t_dry = np.array(avg_t_dry).flatten()
    avg_humidity = np.array(avg_humidity).flatten()

    # get response vars
    avg_elec = np.array(avg_elec).flatten() / c_area / power_norm
    avg_gas = np.array(avg_gas).flatten() / c_area / power_norm

    b_wall_area = b_wall_area * np.ones(avg_elec.size)
    b_win_area = b_win_area * np.ones(avg_elec.size)
    b_vol = b_vol * np.ones(avg_elec.size)
    b_height = b_height * np.ones(avg_elec.size)
    b_floors = b_floors * np.ones(avg_elec.size)
    b_lat, b_long = b_cetroid[0] * np.ones(avg_elec.size), b_cetroid[1] * np.ones(avg_elec.size)
    d_dict = {'b_vol': b_vol, 'b_height': b_height, 'b_floors': b_floors, 'b_win_area': b_win_area,
              'b_wall_area': b_wall_area, 't_dry': avg_t_dry, 'abs_humidity': avg_humidity,
              'b_lat': b_lat, 'b_long': b_long,
              'elec_use': avg_elec, 'gas_use': avg_gas}
    return d_dict


def gen_training_data(time_windows):
    all_avail_ids = bg.building_ids
    print("Available Buildings by ID: ")
    print(all_avail_ids)
    # all_avail_ids = all_avail_ids[[1,50,100,200]]

    training_table = pd.DataFrame()
    for my_build_id in all_avail_ids:
        # gen data
        print("Processing building ID=%d ..." % my_build_id)
        try:
            b_training_dict = get_building_training_data(my_build_id, time_windows)
            b_training_df = pd.DataFrame.from_dict(b_training_dict)
            # append to training data table
            training_table = pd.concat((training_table, b_training_df))
        except:
            pass
    return training_table


def train_model_and_plot(training_table, response):
    predictors = ['b_height', 'b_vol', 'b_win_area', 'b_wall_area', 'b_floors', 't_dry', 'abs_humidity', 'b_lat', 'b_long']
    y = training_table[response].values
    X = training_table[predictors].values

    # split into test/train sets
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.15, random_state=42)

    # Fit regression model
    params = {'n_estimators': 2000, 'max_depth': 4, 'min_samples_split': 2,
              'learning_rate': 0.008, 'loss': 'ls', 'subsample': 0.7, 'verbose': 1}
    clf = ensemble.GradientBoostingRegressor(**params)
    clf.fit(X_train, y_train)

    # get feature importance
    feature_importance = clf.feature_importances_
    print("Relative feature imp:")
    print(np.array([[predictors],[feature_importance]]).T)

    # Plot feature importance
    # make importances relative to max importance
    feature_importance = 100.0 * (feature_importance / feature_importance.max())
    sorted_idx = np.argsort(feature_importance)
    pos = np.arange(sorted_idx.shape[0]) + .5
    plt.subplot(1, 1, 1)
    plt.barh(pos, feature_importance[sorted_idx], align='center')
    plt.yticks(pos, np.array(predictors)[sorted_idx])
    plt.xlabel('Relative Importance')
    plt.title('Variable Importance')
    plt.tight_layout()
    plt.savefig("relative_feature_importance_" + str(response) + ".png")
    plt.close()

    # plot feature interactions
    features = [0, 1, 2, 3, 4, 5, 6, 7, 8, (5, 6), (1,5), (8, 7)]
    for feature_pair in features:
        fig, axs = plot_partial_dependence(clf, X_train, [feature_pair],
                                           feature_names=predictors,
                                           n_jobs=3, grid_resolution=50,
                                           figsize=(5, 4),
                                           dpi=80,
                                           )
        for ax in axs:
            for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels()):
                item.set_fontsize(9)
            ax.yaxis.set_major_formatter(FormatStrFormatter('%.2f'))
            ax.xaxis.set_major_formatter(FormatStrFormatter('%.2e'))

        """
        if response == 'elec_use':
            fig.suptitle('Partial Dependence of Electricity Use on Weather \n'
                         'and Building Features')
        else:
            fig.suptitle('Partial Dependence of Gas Use on Weather \n'
                         'and Building Features')
        plt.subplots_adjust(top=0.9)  # tight_layout causes overlap with suptitle
        """
        plt.tight_layout()
        plt.savefig("partial_feature_importance_" + str(response) + "_" + str(feature_pair) + ".png")
        plt.close()

    # compute test set deviance
    test_score = np.zeros((params['n_estimators'],), dtype=np.float64)

    for i, y_pred in enumerate(clf.staged_predict(X_test)):
        test_score[i] = clf.loss_(y_test, y_pred)

    plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plt.title('Deviance')
    plt.plot(np.arange(params['n_estimators']) + 1, clf.train_score_, 'b-',
             label='Training Set Deviance')
    plt.plot(np.arange(params['n_estimators']) + 1, test_score, 'r-',
             label='Test Set Deviance')
    plt.legend(loc='upper right')
    plt.xlabel('Boosting Iterations')
    plt.ylabel('Deviance')
    plt.savefig("train_test_performance_" + str(response) + ".png")
    plt.close()
    return clf


def boost_predict(gbtr_elec_model, gbtr_gas_model):
    """!
    @brief Make predictions using trained gradient boosted model.
    """
    from scipy.interpolate import Rbf
    weekend_cut = True
    time_windows = [
                    ('5/1/2017', '5/2/2017 00:00:00', 1.0, 'day', weekend_cut),
                   ]
    training_table = gen_training_data(time_windows)
    # pos coords
    b_long, b_lat = training_table['b_long'].values, training_table['b_lat'].values
    b_vol = training_table['b_vol'].values
    # temp at coords
    b_temps = training_table['t_dry'].values
    rbfi = Rbf(b_long, b_lat, b_temps - np.mean(b_temps))
    # create weather grid from rbf fn
    long_bounds = (np.min(b_long), np.max(b_long))  # deg W
    lat_bounds = (np.min(b_lat), np.max(b_lat))  # deg N
    long_coords = np.linspace(long_bounds[0] - 0.001, long_bounds[1] + 0.001, 20)
    lat_coords = np.linspace(lat_bounds[0] - 0.001, lat_bounds[1] + 0.001, 20)
    long_grid, lat_grid = np.meshgrid(long_coords, lat_coords)
    z = rbfi(long_grid.flatten(), lat_grid.flatten())
    plt.figure()
    plt.contourf(long_grid, lat_grid, z.reshape(long_grid.shape), alpha=0.5)
    plt.colorbar()
    plt.grid(ls='-')
    plt.savefig("test_contour.png")
    plt.close()

    # get daily-avg temps and humidity from ohare csv file.
    # source: https://www.wunderground.com/history/montly
    ohare_aprl_2018 = '../data/NOAA_ohare_2015/ohare_aprl_2018.csv'
    dframe = pd.read_csv(ohare_aprl_2018)
    ohare_data = dframe.values
    day_int = ohare_data[:, 0]
    temp_day_avg = (ohare_data[:, 2] - 32.) * (5. / 9.)
    rel_humid_day_avg = (ohare_data[:, 7] + ohare_data[:, 9]) / 2. / 100.
    abs_humid_day_avg = EnergyData.abs_humidity_from_rh(temp_day_avg, rel_humid_day_avg)

    for day_i in range(len(day_int)):
        pass
    # generate street map plot
    import osmnx as ox
    plt.figure()
    G = ox.graph_from_bbox(lat_bounds[1],
                           lat_bounds[0],
                           long_bounds[1],
                           long_bounds[0], network_type='drive')
    G_projected = ox.project_graph(G)
    fig, ax = ox.plot_graph(G_projected)
    plt.tight_layout()
    plt.savefig("city_heat_map.png")
    plt.close()

    # generate daily energy use results
    predictors = ['b_height', 'b_vol', 'b_win_area', 'b_wall_area', 'b_floors', 't_dry', 'abs_humidity', 'b_lat', 'b_long']
    avg_temps, avg_gas, avg_elec = [], [], []
    b_elec_use, b_gas_use = [], []
    for i in range(len(temp_day_avg)):
        temp_avg = temp_day_avg[i]
        eval_temp = lambda longi, lati: rbfi(longi, lati) + temp_avg
        # build X
        t_dry_vec = eval_temp(b_long, b_lat).flatten()
        abs_humid_vec = abs_humid_day_avg[i] * np.ones(len(b_long))
        # deep copy daily training table
        X_dframe = deepcopy(training_table)
        # set cols to new vals
        X_dframe['t_dry'] = t_dry_vec
        X_dframe['abs_humidity'] = abs_humid_vec

        # make energy prediction for all buildings
        day_elec_use = gbtr_elec_model.predict(X_dframe[predictors].values)
        day_gas_use = gbtr_gas_model.predict(X_dframe[predictors].values)
        b_elec_use.append(day_elec_use)
        b_gas_use.append(day_gas_use)

        # predicted city avg gas
        avg_elec.append(np.average(day_elec_use))
        # predicted city avg elec
        avg_gas.append(np.average(day_gas_use))
        # avg temp
        avg_temps.append(temp_avg)

    # create temp, elec, gas vs time trace
    for i in range(len(avg_temps)):
        with sns.axes_style("whitegrid", {"grid.linestyle": '--', "grid.alpha": 0.6}):
            days = np.array(range(len(avg_temps)), dtype=int) + 1
            plt.figure()
            fig, ax1 = plt.subplots()
            ax1.plot(days, avg_temps, c='b', label="Temperature")
            ax1.scatter(days, avg_temps, c='b')
            ax1.set_xlabel("Day of Month [April 2018]")
            ax1.set_ylabel("Day Avg Temperature [C]")
            plt.legend(loc=2)
            ax2 = ax1.twinx()
            ax2.plot(days, avg_elec, c='r', label=r"Predicted Elec EUI $[W/m^2]$")
            ax2.scatter(days, avg_elec, c='r')
            ax2.plot(days, np.clip(avg_gas, 0., 1e12), c='g', label=r"Predicted Gas EUI $[W/m^2]$")
            ax2.scatter(days, avg_gas, c='g')
            ax2.axvline(i + 1)
            plt.legend(loc=1)
            ax2.set_ylabel(r"Predicted Day Avg Energy Use Intensity $[W/m^2]$")
            plt.title("April 2018:  Predicted City Day Average EUI")
            plt.tight_layout()
            plt.savefig("aprl_gbrt_predict_" + str(i) + ".png")
            plt.close()

    for i in range(len(avg_temps)):
        plt.figure()
        fig, ax = plt.subplots()
        elec_use_vals = b_elec_use[i]
        im = plt.imread("city_heat_map.png")
        implot = ax.imshow(im, zorder=1, \
            extent=[long_bounds[0], long_bounds[1], lat_bounds[0], lat_bounds[1]])
        levels = np.linspace(-3, 17, 11)
        cf = ax.contourf(long_grid, lat_grid, z.reshape(long_grid.shape) + avg_temps[i], levels, alpha=0.35, zorder=2,
                         vmin=-3, vmax=17, extend='both')
        v = np.linspace(-3, 17, 11, endpoint=True)
        bcf = plt.colorbar(cf, shrink=0.75)
        sf = ax.scatter(b_long, b_lat, s=b_vol ** (1/3.), c=np.log(elec_use_vals), zorder=3, cmap='hot',
                        vmin=np.log(5.0), vmax=np.log(110.))  # building scatter points
        bsf = plt.colorbar(sf, shrink=0.75)
        bcf.set_label("Dry Bulb Temperature [C]")
        bsf.set_label(r"ln(Elec EUI $[W/m^2]$)")
        ax.set_xlabel(r"Longitude $[^{\circ}W]$")
        ax.set_ylabel("Lattitude")
        plt.grid(ls='-', alpha=0.2)
        plt.title("Day " + str(i + 1))
        plt.tight_layout()
        plt.savefig("city_heat_map_" + str(i) + ".png")
        plt.close()

def main_train(output_file = "building_training_table.h5"):
    # select time windows of interest
    weekend_cut = True
    time_windows = [
                    ('1/1/2017', '1/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('2/1/2017', '2/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('3/1/2017', '3/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('4/1/2017', '4/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('5/1/2017', '5/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('6/1/2017', '6/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('7/1/2017', '7/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('8/1/2017', '8/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('9/1/2017', '9/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('10/1/2017', '10/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('11/1/2017', '11/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ('12/1/2017', '12/17/2017 00:00:00', 1.0, 'day', weekend_cut),
                    ]

    if os.path.isfile(output_file):
        training_table = pd.HDFStore(output_file)['table']
    else:
        training_table = gen_training_data(time_windows)
        # save training table to file
        training_table.to_hdf(output_file, 'table', complevel=7)

    # run electric gradient boosting model
    response = 'elec_use'
    clf_elec = train_model_and_plot(training_table, response)

    # run gas gradient boosting model
    response = 'gas_use'
    clf_gas = train_model_and_plot(training_table, response)

    return clf_elec, clf_gas


if __name__ == "__main__":
    bg = BuildingGenerator()
    gbrt_elec, gbrt_gas = main_train()
    boost_predict(gbrt_elec, gbrt_gas)
