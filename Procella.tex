%%---------------------------------------------------------------------------%%
%% SVD Hybrid Paper
%%
%% Annals of Nuclear Energy
%%---------------------------------------------------------------------------%%

\documentclass[5p, times]{elsarticle}

\usepackage{microtype}
\usepackage[shortcuts]{glossaries}
\usepackage{graphicx}
\usepackage{multirow}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{algorithm}
\usepackage{algorithmic}
%\usepackage{tmadd}
%\usepackage{tmath}
\usepackage[mathcal]{euscript}
\usepackage{color}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{tabularx}
\usepackage{longtable}
\usepackage{siunitx}
\usepackage{lineno}
\usepackage{rotating}
\usepackage[colorlinks]{hyperref}
\usepackage{bm}
\usepackage{subcaption}

%

\makeatletter
\def\ps@pprintTitle{%
 \let\@oddhead\@empty
 \let\@evenhead\@empty
 \def\@oddfoot{}%
 \let\@evenfoot\@oddfoot}
\makeatother
%

%\journal{Annals of Nuclear Energy}

%%---------------------------------------------------------------------------%%
%% ENVIRONMENTS
%%---------------------------------------------------------------------------%%

%\newcommand{\Sn}{$S\!_{N}$}
%\newcommand{\Spn}{$SP\!_{N}$}
%\newcommand{\denovo}{Denovo}
%\newcommand{\shift}{Shift}
%\newcommand{\advantg}{ADVANTG}
%
%\newcommand{\op}[1]{\ensuremath{\hat{#1}}}
%\newcommand{\adj}[1]{\ensuremath{#1^{\dagger}}}
%\newcommand{\vOmega}{\ensuremath{\boldsymbol{\Omega}}}
%\newcommand{\dr}{\ensuremath{d^3\ve{r}}}

%%---------------------------------------------------------------------------%%

%\setacronymstyle{long-short}
%\newacronym{mc}{MC}{Monte Carlo}
%\newacronym{sn}{\Sn}{discrete ordinates}
%\newacronym{spn}{\Spn}{simplied $P\!N$}
%\newacronym{cadis}{CADIS}{Consistent Adjoint-Driven Importance Sampling}
%\newacronym{fwcadis}{FW-CADIS}{Forward-Weighted, Consistent Adjoint-Driven Importance Sampling}

%%---------------------------------------------------------------------------%%
%% FRONT MATTER
%%---------------------------------------------------------------------------%%

\begin{document}

\begin{frontmatter}

  \title{An Exploration of Weather Influence on Urban Energy Use Patterns for the City of Chicago}
  
  \author[ornl]{Will Gurecky}    \ead{william.gurecky@utexas.edu}
  \author[ornl]{J. Austin Ellis} \ead{jaellis2@ncsu.edu}
  \author[ornl]{Zack Taylor} \ead{rtaylo45@vols.utk.edu}
  \author[ornl]{Tommy Moore} \ead{tommy.moore22@gmail.com}

 %  \address[ncsu]{North Carolina State University, 2311 Stinson Dr., Raleigh,
%    NC 27695 U.S.A}
  \address[ornl]{Oak Ridge National Laboratory, 1 Bethel Valley Rd., Oak
    Ridge, TN 37831 U.S.A.}

%  \begin{abstract}
%    Load imbalance plagues domain decomposed Monte Carlo calculations
%    when sources are not uniform. Parallel efficiency for domain
%    decomposed \acl{mc} transport calculations improves through a nonuniform allocation
%    of processors over subdomains. We optimize the allocation with runtime
%    diagnostics collected during a calibration step then complete the full calculation.
%    The diagnostic-based approach is compared to implicit filtering, an optimization
%    algorithm for bound constrained noisy optimization problems. We consider both
%    forward and hybrid radiation transport calculations to measure performance.
%  \end{abstract}

%  \begin{keyword}
%    \texttt Monte Carlo transport \sep load imbalance \sep domain decomposition
%    \sep noisy optimization
%  \end{keyword}

\end{frontmatter}

%%---------------------------------------------------------------------------%%
%% SECTIONS
%%---------------------------------------------------------------------------%%

\section{Introduction}

Exploration into the relative impact various external and internal factors will have on energy consumption leads to a deeper understanding on how to utilize energy more efficiently. In this work we identify building attributes and external variables, such as the outdoor temperature, that influence the energy consumption of a building using the gradient boosting method. Other methods such as linear regression and T-testing are used to identify buildings response to weather conditions.

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.35\textwidth]{figs/fig_1.png}
    \caption{Outdoor temperature versus electricity use for Osaka, Japan \cite{ref6}.}
    \label{fig:1}
  \end{center}
\end{figure}

Urban energy use studies vary by time-scale, included external factors and geographic region of interest. A study focused on temperature, humidity, and cloud cover influence on building energy use was conducted in \cite{ref6} for the city of Osaka. In this study, correlations were shown between temperature, humidity and energy consumption. A change in energy use for the summer and winter months can be seen in Figure~\ref{fig:1}. The authors also noted a difference in energy response behavior between office and residential buildings. Other studies have reported the significant impact urban heat-island effect has on microclimate \cite{ref9} and urban energy consumption \cite{ref8}. Grid resilience and peak load preparedness requires accurate short time-scale load predictions. Recent applications of neural networks have been applied to make these predictions more accurate \cite{ref4}.

\section{Data Extraction and Exploration}

For each building the outdoor temperature, gas, and electricity use time series data was parsed into a HDF5 table for efficient access. Following this, electricity and gas use and outdoor temperature could be plotted as a function of time for any building in the data set and for any desired time period, seen in Figure~\ref{fig:2}. Do to missing data, the presented analysis only uses data from approximately the first 17 days of each month. Noticeable dips in energy use on the weekends were consistent with a simulation of year 2017. This information allows us to separate the impact of the weekends and holidays from standard business day operations on energy use patterns. 

\begin{figure}[htbp]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/fig_2.png}
    \caption{Time versus energy use and outdoor dry-bulb temperature for Building 9597.}
    \label{fig:2}
  \end{center}
\end{figure}

        The buildings' latitude/longitude and building footprint were extracted from the *.xls file. Additional building attributes such as exterior wall area, conditioned volume, and window coverage were extracted from the *.htm EnergyPlus output files. In Section 4, we estimate the relative sensitivity of energy use to each building attribute using a tree ensemble based machine learning technique.


\section{Building Population Statistics}

We visually inspect the relevant data using histograms and scatter plots. We primarily examine the relationship between the outdoor dry-bulb temperature and the buildings' energy use intensity, defined as energy use per area. T-testing was adopted to compare the response of a single building to the entire building population to determine if a given building is anomalous. This study also reveals the energy trends of weekends and business days.
        The energy use, reported in joules, was converted to an energy use rate [Watts] knowing that the data was provided at 10 minute intervals. Next, the building energy use rate was normalized by the conditioned area since we are interested in testing if the buildings' energy use is impacted by external factors, rather than the size of the floorplan. The normalization provides energy consumption per unit floor area in $[W/m^2]$. These units are a common choice amongst the urban energy use literature 
\footnote{Typical building electricity and energy use is reported in energy use intensity ($EUI$) which is a measure of the amount of energy consumed by a building per unit area over an entire year.  Site $EUI$ traditionally carries units of $[kWhrs/m^2]$.  In this work, we report results in units of  $[W/m^2]$.  This is consistent with \cite{ref6}. We require daily, hourly, or monthly average energy use rates, not just annual total energy consumption.} 
\cite{ref1}. Figure~\ref{fig:4} contains the daily averaged electrical energy consumption versus dry-bulb temperature for the summer months.

   \begin{figure}[htbp!]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/fig_4.png}
    \caption{Outdoor dry-bulb temperature versus citywide day-averaged summer electricity use from $6/21/2017 -- 9/22/2017$. {Weekends and Holidays excluded.} All buildings included.}
    	\label{fig:4}
  	\end{center}
	\end{figure}

During the summer months, an $R^2$ value of $0.11$ for the citywide electricity use as a function of outdoor temperature was obtained. The majority of the variance in daily averaged energy use intensity  $(EUI)$ cannot be explained by the outdoor temperature alone. $EUI$ varies greatly by building type \cite{ref1} and building type is not accounted for in Figure~\ref{fig:4}.  For example, given a hospital, school, office building, and hotel of equal size, each will exhibit different $EUI$.  The average outdoor temperature for this time period was about $19C^{\circ}$ ($66F^{\circ}$) and therefore cooling costs are expected to be low.
        Excluding weekend and holiday dates from the data slightly increases the mean $EUI$ by $2.68$ $[W/m^2]$ meaning weekday electrical use is greater than weekend usage, as expected. Additionally, a subtle increase in the slope of the electricity use fit was also observed by excluding weekends. The increase suggests that when the buildings were active and occupied by people the buildings' electrical use became slightly more sensitive to changes in outdoor temperature.
        We consider the linear models: $E_e = T_d\beta_e+E_{0e}$ for electricity use and $E_g = T_d\beta_g+E_{0g}$ for gas.
To check the slope for statistical significance, a Student's T hypothesis test was conducted on the regression line.  The resulting p-value is displayed on the line plot in Figure~\ref{fig:4}.  The null hypothesis is $H_o ~:~ \hat{\beta} = 0$, or that the slope is zero, and the alternate hypothesis is $H_a~:~ |\hat{\beta}| > 0$.
The t-statistic is computed as follows:
$$
	t = \frac{(\hat{\beta} - 0)}{SE_{\beta}},
$$
where
$$
	SE_{\beta} = \sqrt{\frac{(1/(N-2))\sum\limits_i^N(\hat{y}(x_i) - y_i)^2}{\sum\limits_i^N(\bar{x} - x_i)^2}},
$$
with $\hat{y}_i$ being the predicted $y$ value at $x_i$ and $\bar{x}$ being the mean $x$ value. 

With $t~\sim~T_{(n-2)}$, the student's T-distribution with $n-2$ degrees of freedom, the p-value can be computed by evaluating the T-distribution cumulative density:  $p=1-F_{T_(n-2)}(t)$.  If at a given significance level $\alpha$, or the fraction we are willing to accept a Type I error, we reject the null hypothesis if $p < \alpha$. Figure~\ref{fig:4} shows the estimated slope obtained. With a significance level $\alpha = 0.05$, we reject the null hypothesis, since $p < \alpha$, and can conclude that the positive slope of $0.1308 \pm 0.0101[W/m2]/[K]$ is statistically significant.  In other words, despite the low coefficient of determination there is still a statistically significant outdoor dry-bulb temperature influence on the buildings' summer electricity usage.  Individual buildings can be selected from the dataset.  Upon examining single structures, as in Figure~\ref{fig:6}, the quality of the linear fit greatly increased indicated by the improved $R^2$.  
   \begin{figure}[H]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/fig_6.png}
    \caption{Outdoor dry-bulb temperature versus summer daily average electricity use for Building 9604. {\it Weekends and Holidays excluded.}}
    	\label{fig:6}
  	\end{center}
	\end{figure}
Structures with the greatest $EUI$ sensitivity to the dry-bulb temperature are included in Table~\ref{tab:1}.   A histogram of $EUI$ sensitivities is provided in Figure~\ref{fig:5}.

   \begin{figure}[H]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/fig_5.png}
    \caption{Citywide summary of $EUI$ sensitivity to temperature.}
    	\label{fig:5}
  	\end{center}
	\end{figure}

\begin{table}[H]
  \begin{center}
    \begin{tabular}{|r|r|r|r|}
      \toprule
	{\bf Building ID} & $\frac{\partial(EUI)}{\partial T}${\it [W/$m^2$]/[K]} & $\pm 1\sigma$ & $R^2$ \\
      	\midrule
	9604 & 0.3190 & 0.0362 & 0.80 \\
	16395 & 0.2998 & 0.0317 & 0.82 \\
	19 & 0.2844 & 0.0401 & 0.73 \\
	17163 & 0.2814 & 0.0289 & 0.82 \\
	101 & 0.2743 & 0.0263 & 0.84 \\
      	\bottomrule
    \end{tabular}
    \caption{Buildings with highest sensitivity to summer outdoor dry-bulb temperature.}
    \label{tab:1}
  \end{center}
\end{table}

	
Figure~\ref{fig:7} shows temperature versus winter gas use. 
  \begin{figure}[H]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/fig_7.png}
    \caption{Outdoor dry-bulb temperature versus citywide winter gas use. {\it Weekends and Holidays excluded.}}
    	\label{fig:7}
  	\end{center}
	\end{figure}	

The lower band corresponds to buildings which do not rely solely on gas for heating. Upon inspection of the EnergyPlus output files, these building were typically smaller and only used natural gas for hot water heating. An $R^2$ of $0.58$ was computed for the winter outdoor dry-bulb temperature versus citywide gas linear model. When comparing summer electricity use to winter gas use, the coupling is stronger for to the gas use and outdoor temperature relationship. For the buildings studied, electricity use increases as temperatures drop during the winter due to electric heating, as seen in Figure~\ref{fig:8}  

   \begin{figure}[H]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/fig_8.png}
    \caption{Outdoor dry-bulb temperature versus citywide winter electricity use. {\it Weekends and Holidays excluded.}}
    	\label{fig:8}
  	\end{center}
	\end{figure}	

 

\section{Gradient Boosting}

Gradient boosting is a machine learning technique that is useful for creating forward classification and regression models in addition to estimating relative variable importance. This method makes use of an ensemble of trees approach which relies on greedily fitting a sequence of decision tree stumps, known as weak-learners to generate a strong predictor. Overfitting can be mitigated by tuning two hyperparameters: the learning rate which limits relative influence of each successive tree and a subsampling parameter to control the degree of bagging. We use gradients boosting to map the relative importance of a variety of field variables. 

The gradient boosting method fits boosted trees to the data and tallies the amount of splitting in a particular dimension. In addition, the split gain - computed as the reduction in standard error of the prediction for our simple regression case - is tallied alongside the number of splits.  Once all boosted iterations are complete the feature's relative importance can be estimated. Outdoor humidity was computed from wet and dry-bulb temperatures according to a relationship provided by \cite{ref7}.  Figure~\ref{fig:9} shows that humidity is slightly less predictive of electricity use intensity than the temperature or building volume.  

  \begin{figure}[H]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/fig_9.png}
    \caption{Relative electricity $EUI$ feature importance.}
    	\label{fig:9}
  	\end{center}
	\end{figure}
\begin{figure}[H]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/fig_10.png}
    \caption{Relative gas $EUI$ feature importance.}
    	\label{fig:10}
  	\end{center}
	\end{figure}
	

Figures~\ref{fig:11} and \ref{fig:12} show the partial impact of each explanatory variable on both electricity and gas use with all other exogenous variables marginalized out . The partial dependency results show a $1/R$ trend for the building volume's impact on $EUI$, suggesting smaller buildings are less thermally efficient than larger structures due to a high surface area to volume ratio.    
   \begin{figure}[H]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/elec_partial.pdf}
    \caption{Electricity use intensity partial dependence plots.}
    	\label{fig:11}
  	\end{center}
	\end{figure}
   \begin{figure}[H]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/gas_partial.pdf}
    \caption{Gas use intensity partial dependence plots.}
    	\label{fig:12}
  	\end{center}
	\end{figure}	
The humidity is computed as an absolute concentration in $[g/m^3]$.  Colder air cannot support as much water per volume, and therefore the behavior of absolute humidity closely tracks the dry-bulb temperature since low temperatures result in low absolute humidity levels. Partial dependence plots reveal that increasing building size leads to, on average, smaller electricity energy consumption per unit conditioned area. Larger buildings have a greater volume to surface area ratio, and therefore better thermal properties with respect to HVAC efficiency.

We use the model generated by gradient boosting to predict average citywide electricity and gas use for the month of April 2018. Figure~\ref{fig:13} shows the citywide electricity use for April $1^{st}$. Building volume is represented as the vertex size in the city plot. The buildings electric energy intensity is denoted by the vertex color. We note that the smallest structures typically consumed the largest amount of electricity per unit floor area which is consistent with the previous findings. We gather both temperature and humidity from the Chicago O'Hare airport from \cite{wunder}, which are used as inputs to our model. Figure~\ref{fig:14} shows our full month predictions.

   \begin{figure}[H]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/city_heat_map_0.png}
    \caption{Map of Chicago's predicted electricity use for April $1^{st}$ 2018.}
    	\label{fig:13}
  	\end{center}
	\end{figure}	

  \begin{figure}[H]
  \begin{center}
    \includegraphics[width=.49\textwidth]{figs/aprl_gbrt_predict_0.png}
    \caption{Chicago's predicted energy use from gradient boosted model with real April 2018 temperatures and humidities.}
    	\label{fig:14}
  	\end{center}
	\end{figure}

\section{Conclusions}

A series of histograms and linear regressions revealed seasonal, weekend, and monthly trends in energy use. T-tests were conducted on the slopes of the regression models to verify the correlations statistical significance. Additionally, buildings with the highest sensitivity to the outdoor dry-bulb temperature could be detected using this regression approach. Gradient boosting was employed to elucidate the relative explanatory power of a variety of building attributes and external conditions.  Building volume was shown to have a substantial influence on the $EUI$ of any given structure. Using our model, we predict the citywide energy use for both electricity and gas use for April 2018. The fast to evaluate, reduced order model can be useful as a surrogate for on-the-fly grid forecasting and daily building energy use optimization. Future work would include comparing the gradient boosted model with other energy use models, along with using the models to target DOE critical areas of research, such as grid resilience and modernization. 

%%%---------------------------------------------------------------------------%%
%\section*{Acknowledgments}
%%%---------------------------------------------------------------------------%%
%
%Work for this paper was supported by Oak Ridge National Laboratory, which is
%managed and operated by UT-Battelle, LLC, for the U.S. Department of Energy
%under Contract No. DEAC05-00OR22725 and National Science Foundation Grant
%DMS-1638521 to the Statistical and Applied Mathematical Sciences Institute.
%This research used resources of the Oak
%Ridge Leadership Computing Facility at the Oak Ridge National Laboratory,
%which is supported by the Office of Science of the U.S. Department of Energy
%under Contract No. DE-AC05-00OR22725.

%%---------------------------------------------------------------------------%%
\section*{References}

%% `Elsevier LaTeX' style
\bibliographystyle{elsarticle-num}
\bibliography{references}

%%---------------------------------------------------------------------------%%

%\appendix
%\input{appendix}

%%---------------------------------------------------------------------------%%
%% end
%%---------------------------------------------------------------------------%%

\end{document}

%%---------------------------------------------------------------------------%%
%% end of main.tex
%%---------------------------------------------------------------------------%%
